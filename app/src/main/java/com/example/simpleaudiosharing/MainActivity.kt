package com.example.simpleaudiosharing

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.io.OutputStreamWriter
import java.net.DatagramSocket
import java.net.ServerSocket

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val startServer = findViewById<Button>(R.id.start_server)
        startServer.setOnClickListener {
            startActivity(Intent(this, ServerActivity::class.java))
        }

        val connectToServer = findViewById<Button>(R.id.connect_to_server)
        connectToServer.setOnClickListener{
            startActivity(Intent(this, ClientActivity::class.java))
        }
    }
}