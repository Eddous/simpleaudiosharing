package com.example.simpleaudiosharing

data class MainToServer(val filePath: String, val playing: Boolean, val trackTime: Int, val unixTime: Long)

data class ServerToClient(val fileName: String, val playing: Boolean, val trackTime: Int, val unixTime: Long)
