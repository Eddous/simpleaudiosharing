package com.example.simpleaudiosharing

import java.io.DataOutputStream
import java.net.ServerSocket
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.util.concurrent.ArrayBlockingQueue

class Server(private val messageQueue: ArrayBlockingQueue<MainToServer>) : Runnable {

    private val PORT = 1458
    private lateinit var dataOutputStream: DataOutputStream

    override fun run() {
        // debugging
        while(true){
            val message = messageQueue.take()
            println(message)
        }

        val serverSocket = ServerSocket(PORT)
        val clientSocket = serverSocket.accept()
        dataOutputStream = DataOutputStream(clientSocket.getOutputStream())
    }

    private fun syncClient(lastMsg: MainToServer) {
        try {
            val file = File(lastMsg.filePath)
            dataOutputStream.writeLong(file.length())

            FileInputStream(file).use { fileInputStream ->
                val buffer = ByteArray(1024)
                var bytesRead: Int
                while (fileInputStream.read(buffer).also { bytesRead = it } != -1) {
                    dataOutputStream.write(buffer, 0, bytesRead)
                }
            }
            dataOutputStream.flush()

        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            dataOutputStream.close()
        }
    }
}