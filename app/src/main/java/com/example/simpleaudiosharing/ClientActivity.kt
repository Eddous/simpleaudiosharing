package com.example.simpleaudiosharing

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import java.io.BufferedReader
import java.io.InputStreamReader

class ClientActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client)

        val status = findViewById<TextView>(R.id.status)

        val process = Runtime.getRuntime().exec("ip route show default")
        val reader = BufferedReader(InputStreamReader(process.inputStream))
        var line = reader.readLine()
        reader.close()
        if (line == ""){
            line = "no network info"
        }
        status.text = line

        // connect to the server
        // wait for a file name
        // OK -> nothing
        // NOK -> get a file
        // communicate with the server
    }
}