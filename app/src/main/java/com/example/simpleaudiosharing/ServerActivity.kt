package com.example.simpleaudiosharing

import android.app.Activity
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.starry.file_utils.FileUtils
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.ConcurrentLinkedQueue


class ServerActivity : AppCompatActivity() {

    private var player: MediaPlayer = MediaPlayer()
    private var uri: Uri? = null
    private var filePath: String = ""
    private lateinit var server: Server

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server)
        val fileNameField = findViewById<TextView>(R.id.file_name)

        val messageQueue = ArrayBlockingQueue<MainToServer>(10)
        server = Server(messageQueue)
        Thread(server).start()

        var resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == Activity.RESULT_OK) {
                    val data: Intent? = result.data
                    uri = data?.data
                    filePath = FileUtils(this).getPath(uri!!)!!
                    fileNameField.setText(filePath)
                    messageQueue.add(MainToServer(filePath, false, 0, System.currentTimeMillis()))
                }
            }

        findViewById<Button>(R.id.open_file).setOnClickListener {
            val intent = Intent()
                .setType("*/*")
                .setAction(Intent.ACTION_GET_CONTENT)
            resultLauncher.launch(intent)
        }

        findViewById<Button>(R.id.start_stop).setOnClickListener {
            if (uri == null) {
                return@setOnClickListener
            }
            player?.apply {
                setDataSource(applicationContext, uri!!)
                prepare()
                start()
            }
        }
    }
}
